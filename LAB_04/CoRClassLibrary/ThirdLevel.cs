﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoRClassLibrary
{
    public class ThirdLevel : BaseHandler
    {
        protected override string Report => "The most common reason for a phone turning off automatically is due to a low or faulty battery";

        protected override int Level => 3;

        protected override string Problem => "The device turns off automatically";

        public override void Request(UserRequest request)
        {
            if (request.UserProblem.Equals(Problem, StringComparison.CurrentCultureIgnoreCase))
            {
                Console.WriteLine($"Level {Level} support: {this.Report}");
                return;
            }

            base.Request(request);
        }
    }
}
