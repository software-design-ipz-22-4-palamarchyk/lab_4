﻿namespace AirTrafficControlLibrary
{
    public static class Print
    {
        public static void PrintSuccess(string message) =>
       PrintMessage(message);

        public static void PrintError(string message) =>
            PrintMessage(message);

        public static void PrintMessage(string message)
        {
            Console.WriteLine(message);
            Console.ResetColor();
        }

    }

}
